# node-app

# Exercice 1 :

Etape 1 (cloner le repository) : <br><br>
    $ git clone https://gitlab.com/thomas.tilly/node-app.git

Etape 2 (générer l'application) : <br><br>
    $ npx epress-generator --hogan --git <br>
    Puis ajouter les ajouts grâce à git push

Etape 3 (installer les dépendances) : <br><br>
    $ npm install <br>
    $ DEBUG="node-app:server" npm start
    
Le serveur est lancé sur : http://localhost:3000/